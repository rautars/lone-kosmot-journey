package com.xeranas.lkj.util;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class TileMapUtil {

	private static final String PARAM_RESOURCE_NAME = "resName";
	
	public static void registerMapCollisionObjects(World world, MapObjects mapObjects) {
		for (MapObject mapObject : mapObjects) {
			
			if (mapObject instanceof PolylineMapObject) {

				ChainShape chainShape = new ChainShape();
				chainShape.createChain(convertVerticesToBox2dMetric((PolylineMapObject) mapObject));
				
				// Create a fixture definition to apply our shape to
				FixtureDef fixtureDef = new FixtureDef();
				fixtureDef.shape = chainShape;

				BodyDef bodyDef = new BodyDef();
				bodyDef.type = BodyType.StaticBody;
				
				Body body = world.createBody(bodyDef);
				body.createFixture(fixtureDef);
				body.setUserData(Global.COLL_PLATFORM);
				
				chainShape.dispose();
			}
		}
	}
	
	public static void registerMapTextObjects(World world, MapObjects mapObjects) {
		for (MapObject mapObject : mapObjects) {
			
			if (mapObject instanceof EllipseMapObject) {

				float box2dMetricRadius = ((EllipseMapObject) mapObject).getEllipse().height / Global.PIXELS_PER_METER;
				float box2dMetricPositionX = ((EllipseMapObject) mapObject).getEllipse().x / Global.PIXELS_PER_METER;
				float box2dMetricPositionY = ((EllipseMapObject) mapObject).getEllipse().y / Global.PIXELS_PER_METER;
				
				CircleShape circleShape = new CircleShape();
				circleShape.setRadius(box2dMetricRadius);
				circleShape.setPosition(new Vector2(box2dMetricPositionX, box2dMetricPositionY));
				
				// Create a fixture definition to apply our shape to
				FixtureDef fixtureDef = new FixtureDef();
				fixtureDef.shape = circleShape;
				fixtureDef.isSensor = true;
				
				BodyDef bodyDef = new BodyDef();
				bodyDef.type = BodyType.StaticBody;
				
				Body body = world.createBody(bodyDef);
				body.createFixture(fixtureDef);
				
				String name = (String) mapObject.getProperties().get(PARAM_RESOURCE_NAME);
				body.setUserData(new TextBox(name));
				
				circleShape.dispose();
			}
		}		
	}
	
	private static float[] convertVerticesToBox2dMetric(PolylineMapObject polylineMapObject) {
		float[] transformedVertices = polylineMapObject.getPolyline().getTransformedVertices();
		
		for (int i = 0; i < transformedVertices.length; i++) {
			transformedVertices[i] = transformedVertices[i] / Global.PIXELS_PER_METER; 
		}
		
		return transformedVertices;
	}
	
}
