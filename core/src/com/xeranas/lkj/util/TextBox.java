package com.xeranas.lkj.util;

import com.badlogic.gdx.Gdx;

public class TextBox {

	private String text;
	
	public TextBox(String name) {
		text = Gdx.files.internal("texts/en/level1/" + name + ".txt").readString();
	}
	
	public String getText() {
		return text;
	}
}
