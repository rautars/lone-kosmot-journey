package com.xeranas.lkj.util;

import com.badlogic.gdx.math.Vector2;

/** Global constants. */
public class Global {

	// Box2D world <<< 
	
	public static final Vector2 GRAVITY = new Vector2(0, -10);
	public static final float TIME_STEP = 1/60f;
	public static final int VELOCITY_ITERATIONS = 6;
	public static final int POSITION_ITERATIONS = 2;
	public static final float PIXELS_PER_METER = 64;
	public static final String DELETE_ME = "DELETE_ME";
	
	// Box2D world >>>

	
	// Box2d world Camera <<<
	
	public static final float VIEWPORT_WIDTH = 16f;
	public static final float VIEWPORT_HEIGHT = 9f;

	// Box2d world Camera >>>

	
	// UI Camera <<<
	
	public static final float UI_VIEWPORT_WIDTH = 1280f;
	public static final float UI_VIEWPORT_HEIGHT = 720f;

	// UI Camera >>>
	
	
	// Collision object user data <<<
	
	public static final String COLL_PLAYER = "PLAYER";
	public static final String COLL_PLAYER_SENSOR = "PLAYER_SENSOR";
	public static final String COLL_PLATFORM = "PLATFORM";
	
	// Collision object user data >>>
	
	
	// Tiled map editor <<<
	
	public static final String COLL_LAYER = "collision-layer";
	public static final String TEXT_OBJS_LAYER = "text-objects-layer";
	
	// Tiled map editor >>>
}
