package com.xeranas.lkj.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class AudioUtils {
	
	private static Music music;
	
	public static void playBackgroundMusic() {
		
		if (music == null) {
			music = Gdx.audio.newMusic(Gdx.files.internal("audio/273149__tristan-lohengrin__spaceship-atmosphere-04.mp3"));
		}
		
		music.play();
		music.setLooping(true);
	}

	public static void cleanUp() {
		music.dispose();
	}

}
