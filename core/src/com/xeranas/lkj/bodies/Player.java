package com.xeranas.lkj.bodies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.utils.Disposable;
import com.xeranas.lkj.util.Global;
import com.xeranas.lkj.util.MessageQueue;
import com.xeranas.lkj.util.TextBox;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;

public class Player extends InputAdapter implements ContactListener, Disposable{

	private float width = 0.7f;
	private float height = 1.3f;
	
	private Vector2 position;
	private Body body;
	private Body footSensorBody;
	
	private float impulseX = 0f;
	private float impulseY = 0f;
	
	private Texture kosmotImg;
	private TextureRegion kosmotTextureRegion;
	
	private boolean movingForward = false;
	private boolean canJump = true;
	
	private ConeLight coneLight;
	private World world;
	
	public Player(World world, Vector2 position, RayHandler rayHandler) {
		this.position = position;
		this.world = world;
		kosmotImg = new Texture("player/kosmot1.png");
		kosmotImg.setFilter(kosmotImg.getMinFilter(), TextureFilter.Linear);
		kosmotTextureRegion = new TextureRegion(kosmotImg);
		width = kosmotImg.getWidth() / Global.PIXELS_PER_METER;
		height = kosmotImg.getHeight() / Global.PIXELS_PER_METER;

		PointLight pointLight = new PointLight(rayHandler, 10, Color.BLACK, 5f, position.x, position.y + height);
		coneLight = new ConeLight(rayHandler, 20, Color.RED, 5f, position.x, position.y + height/3f, 20f, 55f);
		coneLight.setSoftnessLength(0f);
		
		BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.fixedRotation = true;
        bodyDef.position.set(position.x, position.y);
    	
    	PolygonShape polygonShape = new PolygonShape();
    	polygonShape.setAsBox(width/2f, height/2f);
    	
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
		fixtureDef.density = 0.5f; 
		fixtureDef.friction = 0.6f;
		fixtureDef.restitution = 0f;
        
		body = world.createBody(bodyDef);
		MassData data = new MassData();
		data.mass = 40f;
        body.setMassData(data);
        body.createFixture(fixtureDef);
        body.setUserData(Global.COLL_PLAYER);
        
        pointLight.attachToBody(body);
        polygonShape.dispose();

        // foot sensor for jumping detection
        
    	PolygonShape footSensorShape = new PolygonShape();
    	footSensorShape.setAsBox(width/2.5f, 0.1f);
    	fixtureDef = new FixtureDef();
    	fixtureDef.shape = footSensorShape;
    	fixtureDef.isSensor = true;
    	
    	Vector2 jumpSensorPos = new Vector2(position.x, position.y - height/2f);
    	bodyDef.position.set(jumpSensorPos);
    	footSensorBody = world.createBody(bodyDef);
    	footSensorBody.setUserData(Global.COLL_PLAYER_SENSOR);
    	footSensorBody.createFixture(fixtureDef);
    	
    	footSensorShape.dispose();
    	
    	// connect body with footSensor
    	WeldJointDef jointDef = new WeldJointDef ();
    	jointDef.initialize(body, footSensorBody, jumpSensorPos);
    	world.createJoint(jointDef);
    	
	}
	
	public void update(Batch batch) {
		position = body.getPosition();
		body.applyLinearImpulse(impulseX, impulseY, position.x, position.y, true);
		
		coneLight.setPosition(new Vector2(position.x, position.y + height/3f));
		
		batch.begin();
		batch.draw(kosmotTextureRegion, position.x - width/2f, 
				position.y - height/2f,
				kosmotImg.getWidth()/Global.PIXELS_PER_METER,
				kosmotImg.getHeight()/Global.PIXELS_PER_METER);
		batch.end();
	}
	
	@Override
	public boolean keyDown (int keycode) {
		
		switch (keycode) {
		case Keys.LEFT:
		case Keys.A:
			if (!movingForward) {
				kosmotTextureRegion.flip(true, false);
				coneLight.setDirection(180f - 20f);
				movingForward = true;
			}
			impulseX = -0.40f;
			break;
		case Keys.RIGHT:
		case Keys.D:
			if (movingForward) {
				kosmotTextureRegion.flip(true, false);
				coneLight.setDirection(20f);
				movingForward = false;
			}
			impulseX = 0.40f;
			break;
		case Keys.SPACE:
			Gdx.app.log("Keys.SPACE: canJump?: ", String.valueOf(canJump));
			if (canJump) {
				body.applyForce(0f, 700f, position.x, position.y, true);
			}
			break;
		default:
			break;
		}
		
		return false;
	}
	
	@Override
	public boolean keyUp (int keycode) {
		
		switch (keycode) {
		case Keys.LEFT:
		case Keys.A:
			impulseX = 0f;
			break;
		case Keys.RIGHT:
		case Keys.D:
			impulseX = 0f;
			break;
		default:
			break;
		}
		
		return false;
	}

	@Override
	public void beginContact(Contact contact) {
		if (isContactExistBetweenPlatformAndPlayer(contact)) {
			canJump = true;
		}
		
		checkForTextObjects(contact);
	}

	/** Helper method to detect contact between platform and player sensor. */
	private boolean isContactExistBetweenPlatformAndPlayer(Contact contact) {
		final Object OBJECT_A = contact.getFixtureA().getBody().getUserData();
		final Object OBJECT_B = contact.getFixtureB().getBody().getUserData();
		
		if (Global.COLL_PLATFORM.equals(OBJECT_A) && Global.COLL_PLAYER_SENSOR.equals(OBJECT_B)) {
			return true;
		}
		
		if (Global.COLL_PLATFORM.equals(OBJECT_B) && Global.COLL_PLAYER_SENSOR.equals(OBJECT_A)) {
			return true;
		}
		
		return false;
	}
	
	private void checkForTextObjects(Contact contact) {
		final Object OBJECT_A = contact.getFixtureA().getBody().getUserData();
		final Object OBJECT_B = contact.getFixtureB().getBody().getUserData();

		if (OBJECT_A instanceof TextBox && Global.COLL_PLAYER.equals(OBJECT_B)) {
			MessageQueue.addMessage( ((TextBox) OBJECT_A).getText() );
			contact.getFixtureA().getBody().setUserData(Global.DELETE_ME);
		} else if (OBJECT_B instanceof TextBox && Global.COLL_PLAYER.equals(OBJECT_A)) {
			MessageQueue.addMessage( ((TextBox) OBJECT_B).getText() );
			contact.getFixtureB().getBody().setUserData(Global.DELETE_ME);
		}
	}

	@Override
	public void endContact(Contact contact) {
		if (isContactExistBetweenPlatformAndPlayer(contact)) {
			canJump = false;
		}
	}
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}
	
	public Body getBody() {
		return body;
	}

	@Override
	public void dispose() {
		kosmotImg.dispose();
	}

}
