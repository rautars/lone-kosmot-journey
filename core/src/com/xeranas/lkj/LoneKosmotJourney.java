package com.xeranas.lkj;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.xeranas.lkj.actors.RadioActor;
import com.xeranas.lkj.actors.TextActor;
import com.xeranas.lkj.bodies.Player;
import com.xeranas.lkj.util.AudioUtils;
import com.xeranas.lkj.util.Global;
import com.xeranas.lkj.util.TileMapUtil;

import box2dLight.RayHandler;

public class LoneKosmotJourney extends Game {

	private Box2DDebugRenderer debugRenderer;
	private FPSLogger fpsLogger;
	
	private World world;
	private OrthographicCamera camera;
	private OrthographicCamera uiCamera;
	private OrthographicCamera tileMapCamera;
	
	private Player player;
	private OrthogonalTiledMapRenderer tmr;
	private TiledMap map;
	private Texture backgroundImg;
	private RayHandler rayHandler;
	
	private Stage stage;
	private Stage uiStage;
	
	@Override
	public void create () {
		
		stage = new Stage(new FitViewport(Global.VIEWPORT_WIDTH, Global.VIEWPORT_HEIGHT));
		uiStage = new Stage(new FitViewport(Global.UI_VIEWPORT_WIDTH, Global.UI_VIEWPORT_HEIGHT));
		
		Box2D.init();
		fpsLogger = new FPSLogger();
		debugRenderer = new Box2DDebugRenderer();
		stage.setDebugInvisible(true);

		
		world = new World(Global.GRAVITY, true);
		
		AudioUtils.playBackgroundMusic();
		
		rayHandler = new RayHandler(world);
		rayHandler.setAmbientLight(0.3f);
		
		camera = ((OrthographicCamera) stage.getCamera());
		uiCamera = ((OrthographicCamera) uiStage.getCamera());
		tileMapCamera = new OrthographicCamera();
		
		backgroundImg = new Texture("starmap.png");
		
		player = new Player(world, new Vector2(3f, 6f), rayHandler);
		Gdx.input.setInputProcessor(player);
    	world.setContactListener(player); 
    	
    	TmxMapLoader.Parameters params = new TmxMapLoader.Parameters();
    	params.textureMagFilter = TextureFilter.Linear;
		map = new TmxMapLoader().load("map/level1.tmx", params);
		tmr = new OrthogonalTiledMapRenderer(map, 1/Global.PIXELS_PER_METER);
		TileMapUtil.registerMapCollisionObjects(world, map.getLayers().get(Global.COLL_LAYER).getObjects());
		TileMapUtil.registerMapTextObjects(world, map.getLayers().get(Global.TEXT_OBJS_LAYER).getObjects());
		
		stage.addActor(new RadioActor(camera, -5.5f, -4.3f));
		uiStage.addActor(new TextActor(uiCamera, -6.5f, -3.5f));
	}
	
	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		world.step(Global.TIME_STEP, Global.VELOCITY_ITERATIONS, Global.POSITION_ITERATIONS);
		
		tmr.getBatch().begin();
		tmr.getBatch().draw(backgroundImg, camera.position.x - Global.VIEWPORT_WIDTH/2f, 
				camera.position.y - Global.VIEWPORT_HEIGHT/2f,
				backgroundImg.getWidth()/Global.PIXELS_PER_METER,
				backgroundImg.getHeight()/Global.PIXELS_PER_METER);
		tmr.getBatch().end();
		
		
		Vector2 playerPos = player.getBody().getPosition();
		float lerp = Gdx.graphics.getDeltaTime();
		camera.position.x += (playerPos.x - camera.position.x) * lerp;
		camera.position.y += (playerPos.y - camera.position.y) * lerp;
		
		uiCamera.position.x = camera.position.x;
		uiCamera.position.y = camera.position.y;
		uiCamera.update();
		
		tmr.setView(camera);
		camera.update();
		
        tmr.render();
        player.update(tmr.getBatch());
        
        rayHandler.setCombinedMatrix(camera);
        rayHandler.updateAndRender();        
        

        stage.draw();
        uiStage.draw();
        
        
        debugRenderer.render(world, camera.combined);
        fpsLogger.log();
        
        cleanWorldFromDeadBodies();
	}
	
	@Override
	public void resize (int width, int height) {
		camera.update();
	}
	
	@Override
	public void dispose () {
		super.dispose();
		player.dispose();
		AudioUtils.cleanUp();
		world.dispose();
		backgroundImg.dispose();
		map.dispose();
		tmr.dispose();
		rayHandler.dispose();
	}
	
	public void cleanWorldFromDeadBodies() {
		Array<Body> bodies = new Array<Body>();
		world.getBodies(bodies);
		for (Body body : bodies) {
			if (Global.DELETE_ME.equals(body.getUserData())) {
				world.destroyBody(body);
			}
		}

	}
}
