package com.xeranas.lkj.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.xeranas.lkj.util.Global;

public class RadioActor extends Actor {

	private Sprite sprite;
	private Camera camera;
	private float offsetX;
	private float offsetY;
	
	public RadioActor (Camera camera, float offsetX, float offsetY) {
		this.camera = camera;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		
		Texture texture = new Texture(Gdx.files.internal("dialog_bg2.png"));
		
		sprite = new Sprite(texture);
		sprite.setSize(sprite.getWidth()/Global.PIXELS_PER_METER, 
				sprite.getHeight()/Global.PIXELS_PER_METER);
		
		setBounds(sprite.getX(), sprite.getY(), 
				sprite.getWidth()/Global.PIXELS_PER_METER, 
				sprite.getHeight()/Global.PIXELS_PER_METER);
		
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		sprite.setPosition(camera.position.x + offsetX, camera.position.y + offsetY);
		sprite.draw(batch);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
	}

}
