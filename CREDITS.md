# CODE
- Arturas Norkus - https://github.com/xeranas

# GRAPHICS
- Arturas Norkus - https://github.com/xeranas

# AUDIO
- Background music "Spaceship Atmosphere 04" by Tristan Lohengrin from freesound
https://www.freesound.org/people/Tristan_Lohengrin/sounds/273149/

# FONTS
- Fifteen by Zeimusu - https://fontlibrary.org/en/font/fifteen